package Exception5;

public class Main {

	public static void main(String[] args) {
		Refrigerator rf = new Refrigerator(5);
		System.out.println("Before: "+rf.toString());
		try {
			rf.put("chocolate");
			rf.put("milk");
			rf.put("apple");
			rf.put("orange");
			rf.put("coffee");
			rf.put("cabbage");
			System.out.println(rf.takeOut("milk"));
			System.out.println(rf.takeOut("egg"));
		} catch (FullException e) {
			System.err.println("Refrigerator is already full.");
		}finally{
			System.out.println("");
			System.out.println("After: "+rf.toString());
		}

	}

}
